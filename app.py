from flask import Flask
from flask_cors import CORS
import os

app = Flask(__name__)
CORS(app)

@app.route("/")
def helloworld():
    return '202'
# def do_pull():
#   os.system('git checkout -b master ; git pull ; ')

if __name__ == '__main__':
    app.run(port=8081)